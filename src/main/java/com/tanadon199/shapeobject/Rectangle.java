/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.shapeobject;

/**
 *
 * @author Kitty
 */
public class Rectangle extends Shape{
    protected double width;
    protected double height;
    
    public Rectangle (double width , double height){
        if(width==0){
            System.out.println("width = 0 !");
        }if(height==0){
            System.out.println("height = 0 !");
        }
        this.width=width;
        this.height=height;
    }
    @Override
    public double calArea(){
        return this.width*this.height;
    }
    
    @Override
    public void Show(){
        System.out.println("Rectangle width: "+this.width+" height is: "+this.height+" Area is: "+this.calArea());
    }
    
}
