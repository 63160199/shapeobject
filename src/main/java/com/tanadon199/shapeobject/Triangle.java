/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.shapeobject;

/**
 *
 * @author Kitty
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    
    public Triangle (double base , double height){
        if(base==0 || height==0){
            System.out.println("Base or High is 0 !!");
            return;
        }this.base=base;
        this.height=height;
    }
    @Override
    public double calArea(){
        return (this.base*this.height)/2;
    }
    
    @Override
    public void Show(){
        System.out.println("Trianle base: "+this.base+" height is: "+this.height+" Area is: "+this.calArea());
    }
}
